﻿using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

    namespace PCG.LevelGeneration
    {
        public class Agent : MonoBehaviour
        {
            // the transform of the agent
            private RectTransform _agentPosition;
   
            // the number that will be randomly generated to choose one of 4 the directions
            private int _directionNumber;

            [Header("Agent Settings")]
            [Tooltip("the step value how much the agent walks forward in a direction")]
            [SerializeField]
            private float _stepSize = 0.1f;
            [Tooltip("adjust the speed of the agent")]
            [Range(0.1f,1)] [SerializeField] private float _movementSpeed;
            [Range(1,99)][SerializeField]
            [Tooltip("determine how much the grid should carve out of the room in percent")]
            private int _carveOut;
            public int CarveOut => _carveOut;
    
            [Header("Randomness Settings")]
            [Tooltip("decide to use a Seed to generate always the same level")]
            [SerializeField] private bool _useSeed = false;
            [Tooltip("set the seed to generate a fixed level")]
            [SerializeField] private int _setSeed = 0;
            
            [Header("Levy Flight")]
            [Tooltip("activates the Levy flight feature")]
            [SerializeField] private bool _activateLevyFlight = false;
            [Tooltip("the min seconds how long the Levy Flight needs to restart")]
            [SerializeField] private float _minSecRestartLevy = 3;
            [Tooltip("the max seconds how long the Levy Flight needs to restart")]
            [SerializeField] private float _maxSecRestartLevy = 5;
            [Tooltip("the min seconds how long the agent will move straight")]
            [SerializeField] private float _minSecToGoStraight = 0.3f;
            [Tooltip("the max seconds how long the agent will move straight")]
            [SerializeField] private float _maxSecToGoStraight = 1;

            [Header("Sphere Overlap Settings")]
            [Tooltip("the sphere position at the top of the agent")]
            [SerializeField] private Transform _sphereTopPosition;
            [Tooltip("the sphere position at the right side of the agent")]
            [SerializeField] private Transform _sphereRightPosition;
            [Tooltip("the sphere position at the bot side of the agent")]
            [SerializeField] private Transform _sphereBotPosition;
            [Tooltip("the sphere position at the left side of the agent")]
            [SerializeField] private Transform _sphereLeftPosition;

            // the bool checks if there is still a grid where the agent can move on
            private bool _allowedToMove = true; 
    
            //the bool checks if the field was already entered of the agent
            private bool _alreadyplaced = false;
    
            // that is for the levy flight feature, when the agent move a straight line to build a new room or to generate a corridor 
            private bool _blockRandom = false;

            // the collider of the agent
            private Collider _agentCollider;

            [Header("Instant Level generation")]
            [Tooltip("the cell that will be instant spawned")]
            [SerializeField] private GameObject _instantGenerationCells;
            [Tooltip("decide to use the instant or animated generation")]
            [SerializeField] private bool _useInstantCreation;
    
            // the grid generator for the cells
            private GridGenerator _gridGenerator;
    
            void Start()
            {
                // if the seed is used, it will be set
                if (_useSeed)
                {
                    Random.InitState(_setSeed);
                }

                // get the agent transform
                _agentPosition = GetComponent<RectTransform>();

                // ge the collider of the agent
                _agentCollider = GetComponent<Collider>();

                // at the start the agent will start to move in a direction
                _directionNumber = Random.Range(0, 4);

                // activates the Levy Flight
                if (_activateLevyFlight)
                {
                    StartCoroutine(StraightPath());
                }

                // find the generator in the scene
                _gridGenerator = FindObjectOfType<GridGenerator>();

                // if the designer uses the instant level generator
                if (_useInstantCreation)
                {
                    // set the best settings for the generation
                    _movementSpeed = 1;
                    _stepSize = 0.15f;
                    // generate the level, until the carve out rate was reached
                    while (_gridGenerator.GroundCounter * 100 / _gridGenerator.CellsCounter <= _carveOut)
                    {
                        MoveAgent();
                    }
                }

            }

            void Update()
            {
                // if the instant creation is not used, use the animated method
                if (!_useInstantCreation)
                {
                    MoveAgent();
                }
            }
    
            /// <summary>
            /// create a new room or corridor
            /// </summary>
            /// <returns></returns>
            IEnumerator StraightPath()
            {
                // wait to activate the Levy Flight
                yield return new WaitForSeconds(Random.Range(_minSecRestartLevy, _maxSecRestartLevy));
                // choose a number to know the direction, where the corridor should be created
                float pathNumber = Random.Range(0, 4);
                // decide if a corridor or single room should be created, if the agent find some free space in the direction
                float skipPathGeneration = Random.Range(0, 2);
        
                // decide to skip cells or not
                if (skipPathGeneration == 0)
                {
                    _agentCollider.enabled = false;
                }
        
                // move to the top side and blocks the other directions
                if (pathNumber == 0)
                {
                    _directionNumber = 0;
                    _blockRandom = true;
            
                }
                // move to the right side and blocks the other directions
                if (pathNumber == 1)
                {
                    _directionNumber = 1;
                    _blockRandom = true;
                }
                // move to the bot side and blocks the other directions
                if (pathNumber == 2)
                {
                    _directionNumber = 2;
                    _blockRandom = true;
                }
                // move to the left side and blocks the other directions
                if (pathNumber == 3)
                {
                    _directionNumber = 3;
                    _blockRandom = true;
                }
                // after a fixed time the agent can move in all directions again
                yield return new WaitForSeconds(Random.Range(_minSecToGoStraight, _maxSecToGoStraight));
                // disabled the block to generate random direction again
                _blockRandom = false;
                // activate the agent collider if it was deactivated to create rooms again
                _agentCollider.enabled = true;
        
                // restart the method
                StartCoroutine(StraightPath());
            }
    
            /// <summary>
            /// move the agent in a random direction
            /// </summary>
            private void MoveAgent()
            {
                // move the agent to the top side
                if (_directionNumber == 0)
                {
                    // get all colliders of the top sphere
                    Collider[] grid = Physics.OverlapSphere(_sphereTopPosition.position, 0.1f, LayerMask.GetMask("Grid"));
           
                    // check if the next move of the agent is still in the grid, if so move it to the top side
                    for (int i = 0; i < grid.Length ; i++)
                    {
                        if (grid[i].CompareTag("Ground") || grid[i].CompareTag("Wall"))
                        {
                            _allowedToMove = true;
                        }
                    }
                   
                    // if the agent is out of bounds, the movement will be stopped
                    if(grid.Length == 0)
                    {
                        _allowedToMove = false;
                    }
           
                    // move the player to the top side, if there is still the grid
                    if(_allowedToMove)
                    {
                        if (_useInstantCreation)
                        {
                            // move the agent to the desired direction
                            _agentPosition.position = new Vector2(transform.position.x,transform.position.y + _stepSize);
                   
                            // get all colliders of the cells, that the agent moves over
                            Collider[] cell = Physics.OverlapSphere(transform.position, 0.05f, LayerMask.GetMask("Grid"));
           
                            // check if the next move of the agent is still on a "wall" field, so that the percentage will be reached, otherwise he won't move on the field
                            for (int i = 0; i < cell.Length ; i++)
                            {
                                // if the cell has the tag "Ground", enter the field
                                if (cell[i].CompareTag("Ground"))
                                {
                                    _alreadyplaced = true;
                                }
                                // if not, then don't enter the field
                                else
                                {
                                    _alreadyplaced = false;
                                }
                            }

                            // if the agent has already placed a cell there, then it will skip the spawn and won't count it as field
                            if (!_alreadyplaced)
                            {
                                // spawn the new field
                                Instantiate(_instantGenerationCells, transform.position, Quaternion.identity);
                                // count the "Ground" fields
                                _gridGenerator.GroundCounter++;
                            }
                        }
               
                        // if the instant generation is not used, then the animated movement will be accessed
                        else
                        {
                            _agentPosition.position = new Vector2(transform.position.x,transform.position.y + _stepSize * _movementSpeed);
                        }
                    }
                }
        
                // move the agent to the right side
                else if  (_directionNumber == 1)
                {
                    // get all colliders of the right sphere
                    Collider[] grid = Physics.OverlapSphere(_sphereRightPosition.position, 0.1f, LayerMask.GetMask("Grid"));

                    // check if the next move of the agent is still in the grid, if so move it to the right side
                    for (int i = 0; i < grid.Length ; i++)
                    {
                        if ( grid[i] != null && grid[i].CompareTag("Ground") || grid[i].CompareTag("Wall"))
                        {
                            _allowedToMove = true;
                        }
                    }
           
                    // if the agent is out of bounds, the movement will be stopped
                    if(grid.Length == 0)
                    {
                        _allowedToMove = false;
                    }
           
           
                    // move the player to the right side, if there is still the grid
                    if (_allowedToMove)
                    {
                        if (_useInstantCreation)
                        {
                            // move the agent to the desired direction
                            _agentPosition.position = new Vector2(transform.position.x + _stepSize,transform.position.y);
                   
                            // get all collider of the cells, that the agent moves over
                            Collider[] cell = Physics.OverlapSphere(transform.position, 0.05f, LayerMask.GetMask("Grid"));
           
                            // check if the next move of the agent is still on a "wall" field, so that the percentage will be reached, otherwise he won't move on the field
                            for (int i = 0; i < cell.Length ; i++)
                            {
                                // if the cell has the tag "Ground", enter the field
                                if (cell[i].CompareTag("Ground"))
                                {
                                    _alreadyplaced = true;
                                }
                                // if not, then don't enter the field
                                else
                                {
                                    _alreadyplaced = false;
                                }
                            }
                            // if the agent has already placed a cell there, then it will skip the spawn and won't count it as field
                            if (!_alreadyplaced)
                            {
                                // spawn the new field
                                Instantiate(_instantGenerationCells, transform.position, Quaternion.identity);
                                // count the "Ground" fields
                                _gridGenerator.GroundCounter++;
                            }
                        }
                        // if the instant generation is not used, then the animated movement will be accessed
                        else
                        {
                            _agentPosition.position = new Vector2(transform.position.x + _stepSize * _movementSpeed,transform.position.y);
                        }

                    }
                }
        
                // move the agent to the bot side
                else if (_directionNumber == 2)
                {
                    // get all colliders of the cells, that the agent moves over
                    Collider[] grid = Physics.OverlapSphere(_sphereBotPosition.position, 0.1f, LayerMask.GetMask("Grid"));

                    // check if the next move of the agent is still in the grid, if so move it to the bot side
                    for (int i = 0; i < grid.Length ; i++)
                    {
                        if (grid[i].CompareTag("Ground") || grid[i].CompareTag("Wall"))
                        {
                            _allowedToMove = true;
                        }
                    }
           
                    // if the agent is out of bounds, the movement will be stopped
                    if(grid.Length == 0)
                    {
                        _allowedToMove = false;
                    }
           
                    // move the player to the left side, if there is still the grid
                    if (_allowedToMove)
                    {
                        if (_useInstantCreation)
                        {
                            // move the agent to the desired direction
                            _agentPosition.position = new Vector2(transform.position.x,transform.position.y - _stepSize);
                   
                            // get all colliders of the cells, that the agent moves over
                            Collider[] cell = Physics.OverlapSphere(transform.position, 0.05f, LayerMask.GetMask("Grid"));
           
                            // check if the next move of the agent is still on a "wall" field, so that the percentage will be reached, otherwise he won't move on the field
                            for (int i = 0; i < cell.Length ; i++)
                            {
                                // if the cell has the tag "Ground", enter the field
                                if (cell[i].CompareTag("Ground"))
                                {
                                    _alreadyplaced = true;
                                }
                                // if not, then don't enter the field
                                else
                                {
                                    _alreadyplaced = false;
                                }
                            }
                            // if the agent has already placed a cell there, then it will skip the spawn and won't count it as field
                            if (!_alreadyplaced)
                            {
                                // spawn the new field
                                Instantiate(_instantGenerationCells, transform.position, Quaternion.identity);
                                // count the "Ground" fields
                                _gridGenerator.GroundCounter++;
                            }
                        }
                        // if the instant generation is not used, then the animated movement will be accessed
                        else
                        {
                            _agentPosition.position = new Vector2(transform.position.x,transform.position.y - _stepSize * _movementSpeed);
                        }
              
                    }
                }
        
                // move the agent to the left side
                else if (_directionNumber == 3)
                {
                    // get all colliders of the left sphere
                    Collider[] grid = Physics.OverlapSphere(_sphereLeftPosition.position, 0.1f, LayerMask.GetMask("Grid"));

                    // check if the next move of the agent is still in the grid, if so move it to the left side
                    for (int i = 0; i < grid.Length ; i++)
                    {
                        if (grid[i].CompareTag("Ground") || grid[i].CompareTag("Wall"))
                        {
                            _allowedToMove = true;
                        }
                    }

                    // if the agent is out of bounds, the movement will be stopped
                    if(grid.Length == 0)
                    {
                        _allowedToMove = false;
                    }
                    // move the player to the left side, if there is still the grid
                    if (_allowedToMove)
                    {
                        if (_useInstantCreation)
                        {
                            // move the agent to the desired direction
                            _agentPosition.position = new Vector2(transform.position.x -_stepSize, transform.position.y);
                   
                            // get all colliders of the cells, that the agent moves over
                            Collider[] cell = Physics.OverlapSphere(transform.position, 0.05f, LayerMask.GetMask("Grid"));

                            // check if the next move of the agent is still on a "wall" field, so that the percentage will be reached, otherwise he won't move on the field
                            for (int i = 0; i < cell.Length ; i++)
                            {
                                // if the cell has the tag "Ground", enter the field
                                if (cell[i].CompareTag("Ground"))
                                {
                                    _alreadyplaced = true;
                                }
                                // if not, then don't enter the field
                                else
                                {
                                    _alreadyplaced = false;
                                }
                            }
                            // if the agent has already placed a cell there, then it will skip the spawn and won't count it as field
                            if (!_alreadyplaced)
                            {
                                // spawn the new field
                                Instantiate(_instantGenerationCells, transform.position, Quaternion.identity);
                                // count the "Ground" fields
                                _gridGenerator.GroundCounter++;
                            }
                        }
                        // if the instant generation is not used, then the animated movement will be accessed
                        else
                        {
                            _agentPosition.position = new Vector2(transform.position.x -_stepSize * _movementSpeed, transform.position.y);
                        }
               
                    }
                }
        
                // if the block is deactivated the agent will be moved in random directions
                if (!_blockRandom)
                {
                    // generate the random number for the corresponding direction
                    _directionNumber = Random.Range(0, 4);
                }

            }

            /// <summary>
            /// Drawn Gizmos for the clearness
            /// </summary>
            private void OnDrawGizmos()
            {
                Gizmos.DrawWireSphere(_sphereTopPosition.position, 0.1f);
                Gizmos.DrawWireSphere(_sphereRightPosition.position, 0.1f);
                Gizmos.DrawWireSphere(_sphereBotPosition.position, 0.1f);
                Gizmos.DrawWireSphere(_sphereLeftPosition.position, 0.1f);
                Gizmos.DrawWireSphere(transform.position, 0.05f);
            }
        }
    }


