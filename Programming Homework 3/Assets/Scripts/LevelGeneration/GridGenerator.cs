﻿using UnityEngine;

namespace PCG.LevelGeneration
{
    public class GridGenerator : MonoBehaviour
    {
        [Header("Grid Settings")] 
        [Tooltip("the start position of the grid on the x axis")] [SerializeField]
        private float _xStartPosition;
        [Tooltip("the start position of the grid on the y axis")] [SerializeField]
        private float _yStartPosition;
        [Tooltip("the length of the columns")] [SerializeField]
        private int _columnLength;
        [Tooltip("the length of the rows")] [SerializeField]
        private int _rowLength;
        [Tooltip("the space between each cell on the x axis")] [SerializeField]
        private float _xSpace;
        [Tooltip("the space between each cell on the y axis")] [SerializeField]
        private float _ySpace;
        [Tooltip("the cell prefab that should spawn")] [SerializeField]
        private GameObject _prefab;
        [Tooltip("the spawn position in the hierarchy")] [SerializeField]
        private GameObject _hierarchySpawnPosition;

        // the agent for the grid
        private Agent _agent;
    
        // the numbers of cells
        private int _cellsCounter;
        public int CellsCounter
        {
            get => _cellsCounter;
            set => _cellsCounter = value;
        }

        // the numbers of the cell which are marked as "Ground"
        private int _groundCounter;
        public int GroundCounter
        {
            get => _groundCounter;
            set => _groundCounter = value;
        }
    
        private void Awake()
        {
            // NOTE
            // the spawn calculation is from this Video: https://www.youtube.com/watch?v=WJimYq2Tczc.
            // It was a struggle to get anything done with the Grid, actually it was more difficult as to code the agent. I first would have done it with an image and would just change the pixels with the agent
            // but that wouldn't fulfill the task and don't know how nice that would run, because I couldn't use collider to mark something like a wall or cell.
            for (int i = 0; i < _columnLength + _rowLength; i++)
            {
                Vector3 position; 
                position = new Vector3(_xStartPosition + (_xSpace * (i % _columnLength)), _yStartPosition + (_ySpace * (i / _columnLength)));
                GameObject images = Instantiate(_prefab, position, Quaternion.identity);
                images.transform.SetParent(_hierarchySpawnPosition.transform);
            }

            // find the agent
            _agent = FindObjectOfType<Agent>();
        }
    
        private void Update()
        {
            // calculate the percent of the carve out grid
            // if it reached a fixed percent, the agent will be destroyed to finish the process
            if(_groundCounter * 100 / _cellsCounter >= _agent.CarveOut && _agent != null)
            {
                Destroy(FindObjectOfType<Agent>().gameObject);
            }
        }
    }
}