﻿using UnityEngine;

// this script is for the instant generation. When the cellars are instant spawning, the old ones beneath the new one gets destroyed
namespace PCG.LevelGeneration
{
    public class DestroyOldCells : MonoBehaviour
    {
        void Start()
        {
            // get the cell beneath the new one
            Collider[] wall = Physics.OverlapSphere(transform.position, 0.1f, LayerMask.GetMask("Grid"));

            for (int i = 0; i < wall.Length; i++)
            {
                // if the cell has the tag "Wall", destroy it
                if (wall[i].CompareTag("Wall"))
                {
                    Destroy(wall[i].gameObject);
                }
            }
        }
    }
}
