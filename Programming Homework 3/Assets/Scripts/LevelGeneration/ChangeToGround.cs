﻿using UnityEngine;

namespace PCG.LevelGeneration
{
    public class ChangeToGround : MonoBehaviour
    {
        // the sprite of the cell 
        [Tooltip("the renderer for the cell")]
       [SerializeField] private SpriteRenderer _cellSprite;
    
        // the grid generator to have a grid
        private GridGenerator _gridGenerator;
        
        void Start()
        {
            // find the object of the grid generator
            _gridGenerator = FindObjectOfType<GridGenerator>();
        
            // add the cell to the cell counter
            _gridGenerator.CellsCounter++;

        }

        private void OnTriggerEnter(Collider other)
        {
            // if the agent reached the cell, it will be white
            // the white cell get the "Ground" Tag and add it to the ground cell counter
            if (other.CompareTag("Agent"))
            {
                if (_cellSprite.color == Color.white)
                    return;
                _gridGenerator.GroundCounter++;
                _cellSprite.tag = "Ground";
                _cellSprite.color = Color.white;
            }
        }
    }
}


